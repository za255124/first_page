import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ContentLayoutComponent } from './content-layout/content-layout.component';


const routes: Routes = [
  // {
  //   path: 'content',
  //   component: ContentLayoutComponent
  // },
  //   {
  //     path: '**',
  //     redirectTo: '',
  //     pathMatch: 'full',
  // }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
